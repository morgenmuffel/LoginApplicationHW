//
//  ViewController.swift
//  LoginApplicationHW
//
//  Created by Maksim on 08.10.2023.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    private var userName = "User"
    private var password = "Password"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 //       guard segue.identifier == "logIn" else { return }
        guard let welcomeVC = segue.destination as? HelloViewController else { return }
        welcomeVC.userName = userNameTF.text
    }
    
    @IBAction func loginButtonPressed() {
        guard
            userNameTF.text == userName,
            passwordTF.text == password
        else {
            showAlert(title: "Неправильное имя или пароль",
                      message: "Пожалуйста, введите корректные данные!",
                      textField: passwordTF)
            return
        }
        
        performSegue(withIdentifier: "logIn", sender: nil)
    }
    
    @IBAction func forgotUserNameButton() {
        showAlert(title: "Упс!",
                  message: "Ваше имя: \(userName) 😉")
    }
    
    @IBAction func forgotPasswordButton() {
        showAlert(title: "Упс!",
                  message: "Ваш пароль: \(password) 😉")
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        userNameTF.text = nil
        passwordTF.text = nil
    }
}

extension LoginViewController {
    private func showAlert(title: String, message: String, textField: UITextField? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            textField?.text = nil
        }
        alert.addAction(okAction)
        present(alert, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super .touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTF {
            textField.resignFirstResponder()
            passwordTF.becomeFirstResponder()
        } else {
            loginButtonPressed()
        }
        return true
    }
}
