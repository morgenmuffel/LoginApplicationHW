//
//  HelloViewController.swift
//  LoginApplicationHW
//
//  Created by Maksim on 08.10.2023.
//

import UIKit

class HelloViewController: UIViewController {
    
    @IBOutlet var welcomeLabel: UILabel!
    
    var userName: String?
    
    private let firstColor = UIColor(red: 100/255, green: 150/255, blue: 50/255, alpha: 1)
    private let secondColor = UIColor(red: 50/255, green: 78/255, blue: 110/255, alpha: 0.8)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addVerticalGradientLayer(topColor: firstColor, bottomColor: secondColor)
        welcomeLabel.text = "Привет, \(userName ?? "Noname")!"
    }
}

// MARK: - Set background color
extension HelloViewController {
    func addVerticalGradientLayer(topColor: UIColor, bottomColor: UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        view.layer.insertSublayer(gradient, at: 0)
    }
}
